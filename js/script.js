/* Add New Divs */
	$(document).ready(function addElements(){
		var i=1;
        var j=4;
    
        for (i=1; i<=4; i++) {
		    $("#actionButton").click(function(){

		    	//adding DIVs
		        $("#deploy-area").append("<div class=\"item col-lg-3 col-md-6 col-sm-6 col-xs-6\"><div class=\"white-area new\"></div></div>");

		        //random background color
				var colors = ["#f4b2b2","#d9f699","#f6bb74","#9bdef1"];                
				var rand = Math.floor(Math.random()*colors.length);           
				$('.new').css("background-color", colors[rand]);

				//random text generator
				 var quotes = new Array("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Maecenas id lobortis arcu. Vestibulum ante ipsum primis in faucibus.", "Nam sit amet luctus mauris, et mattis ex. Nam id urna congue, egestas quam sit amet,rit sit amet.", "Donec nunc nulla, ultricies id tortor at, tristique cursus velit."),
			    randno = quotes[Math.floor( Math.random() * quotes.length )];
			    $('.new').text( randno );

			    //dialog when area is clicked
			    $('.new').click( function() { alert( randno ); });
		    });
		}
	});

/* Sticky Navbar - navbar changes to fixed position after scroll down */
	var num = 102; //number of pixels before modifying styles

	$(window).bind('scroll', function () {
	    if ($(window).scrollTop() > num) {
	        $('.navbar').addClass('fixed');
	    } else {
	        $('.navbar').removeClass('fixed');
	    }
	});

/* Closes menu after click on mobile */
$('.navbar-collapse a').click(function (e) {
    if ($(window).width() < 767) {
        $('.navbar-collapse').collapse('toggle');
    }
});

/* Corrects navbar active link (does not work without this!) */
$(".nav a").on("click", function(){
   $(".nav").find(".active").removeClass("active");
   $(this).parent().addClass("active");
});

/* Smooth Scrolling */
	$(function smoothScrolling() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top-40
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});